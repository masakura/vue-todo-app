import { v4 as uuidv4 } from 'uuid';

export default class Todo {
  /**
   * @param {{text: string}} content
   */
  constructor(content) {
    this.content = { ...content };
    /** @type {string} */
    this.id = uuidv4();
    /** @type {boolean} */
    this.isDone = false;
  }

  /**
   * @returns {string}
   */
  get text() {
    return this.content.text;
  }

  done() {
    this.isDone = true;
  }
}
