import Todo from '@/models/todo/todo';

export default class TodoCollection {
  constructor() {
    /** @type {Todo[]} */
    this.items = [];
  }

  /**
   * @returns {number}
   */
  get count() {
    return this.items.length;
  }

  /**
   * @param {{text: string}} todo
   */
  add(todo) {
    this.items.unshift(new Todo(todo));
  }

  /**
   * @param {number} index
   * @returns {Todo}
   */
  elementAt(index) {
    return this.items[index];
  }

  /**
   * @param {string} id
   */
  remove(id) {
    this.items = this.items.filter((item) => item.id !== id);
  }

  /**
   * @param {string} id
   */
  done(id) {
    const todo = this.items.find((item) => item.id === id);
    todo.done();
  }

  /**
   * @returns {Todo[]}
   */
  allTodos() {
    return this.items;
  }

  /**
   * @returns {Todo[]}
   */
  undoneTodos() {
    return this.items.filter((item) => !item.isDone);
  }
}
