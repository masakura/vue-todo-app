export default class TodoInput {
  constructor() {
    /** @type {string} */
    this.text = '';
  }

  /**
   * @returns {boolean}
   */
  canSubmit() {
    return Boolean(this.text.trim());
  }

  clear() {
    this.text = '';
  }
}
