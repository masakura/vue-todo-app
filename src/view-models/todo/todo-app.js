import { TodoCollection } from '@/models/todo';
import TodoInput from '@/view-models/todo/todo-input';

export default class TodoApp {
  constructor() {
    this.input = new TodoInput();
    this.items = new TodoCollection();
    this.listMode = 'all';
  }

  /**
   * @returns {number}
   */
  get count() {
    return this.items.count;
  }

  /**
   * @returns {Todo[]}
   */
  get list() {
    switch (this.listMode) {
      case 'all': return this.items.allTodos();
      case 'undone': return this.items.undoneTodos();

      default: throw new Error();
    }
  }

  /**
   * @param {{text: string}} todo
   */
  add(todo) {
    return this.items.add(todo);
  }

  /**
   * @param {string} id
   */
  done(id) {
    this.items.done(id);
  }

  /**
   * @param {string} id
   */
  remove(id) {
    this.items.remove(id);
  }

  submit() {
    if (!this.input.canSubmit()) return;

    this.items.add(this.input);
    this.input.clear();
  }

  /**
   * @param {number} index
   * @returns {Todo}
   */
  elementAt(index) {
    return this.items.elementAt(index);
  }

  showAll() {
    this.listMode = 'all';
  }

  showUndone() {
    this.listMode = 'undone';
  }
}
