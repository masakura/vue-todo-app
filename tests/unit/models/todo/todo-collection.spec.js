import { TodoCollection } from '@/models/todo';

describe('TodoCollection', () => {
  /** @type {TodoCollection} */
  let target;

  beforeEach(() => {
    target = new TodoCollection();
  });

  describe('追加', () => {
    test('Todo を一つ追加', () => {
      target.add({ text: 'ニンジンを買う' });

      const todo = target.elementAt(0);

      expect({ text: todo.text }).toEqual({ text: 'ニンジンを買う' });
    });

    test('Todo は最新のものから上', () => {
      target.add({ text: '1' });
      target.add({ text: '2' });
      target.add({ text: '3' });

      const actual = target.allTodos().map((item) => item.text);

      expect(actual).toEqual(['3', '2', '1']);
    });
  });

  describe('Id の割り当て', () => {
    test('Id が割り当てられる', () => {
      target.add({ text: 'ニンジンを買う' });

      const todo = target.elementAt(0);

      expect(todo.id).toBeTruthy();
    });

    test('割り当てた id は毎回変わる', () => {
      target.add({ text: '1' });
      target.add({ text: '2' });

      const first = target.elementAt(0);
      const second = target.elementAt(1);

      expect(first.id).not.toEqual(second.id);
    });
  });

  describe('削除', () => {
    beforeEach(() => {
      target.add({ text: '1' });
      target.add({ text: '2' });
      target.add({ text: '3' });
    });

    test('Todo を削除', () => {
      target.remove(target.elementAt(1).id);

      const actual = target.allTodos().map((item) => item.text);

      expect(actual).not.toContain('2');
    });
  });

  describe('完了', () => {
    beforeEach(() => {
      target.add({ text: '1' });
      target.add({ text: '2' });
      target.add({ text: '3' });
    });

    test('Todo を完了にする', () => {
      target.done(target.elementAt(1).id);

      const actual = target.allTodos().map((item) => ({ isDone: item.isDone, text: item.text }));

      expect(actual).toEqual([
        { isDone: false, text: '3' },
        { isDone: true, text: '2' },
        { isDone: false, text: '1' },
      ]);
    });

    test('完了したもののみ', () => {
      target.done(target.elementAt(1).id);

      const actual = target.undoneTodos().map((item) => item.text);

      expect(actual).toEqual(['3', '1']);
    });
  });
});
