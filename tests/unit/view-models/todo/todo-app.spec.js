import { TodoApp } from '@/view-models/todo';

describe('TodoApp', () => {
  /** @type {TodoApp} */
  let target;

  beforeEach(() => {
    target = new TodoApp();
  });

  describe('追加', () => {
    test('Todo を一つ追加', () => {
      target.input.text = 'ニンジンを買う';
      target.submit();

      const todo = target.elementAt(0);

      expect(todo.text)
        .toEqual('ニンジンを買う');
    });

    test('Todo 追加されたら、入力はクリアされる', () => {
      target.input.text = 'ニンジンを買う';
      target.submit();

      expect(target.input.text)
        .toEqual('');
    });

    test('空の Todo は追加されない', () => {
      target.input.text = '  ';
      target.submit();

      expect(target.count)
        .toEqual(0);
    });
  });

  describe('一覧の表示', () => {
    beforeEach(() => {
      target.add({ text: '1' });
      target.add({ text: '2' });
      target.add({ text: '3' });

      target.done(target.elementAt(1).id);
    });

    test('全て', () => {
      target.showAll();

      const actual = target.list.map((item) => item.text);

      expect(actual).toEqual(['3', '2', '1']);
    });

    test('未完了のみ', () => {
      target.showUndone();

      const actual = target.list.map((item) => item.text);

      expect(actual).toEqual(['3', '1']);
    });
  });
});
